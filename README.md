# InfiniTime

This is a go library for interfacing with InfiniTime firmware
over BLE on Linux.

[![Go Reference](https://pkg.go.dev/badge/go.arsenm.dev/infinitime.svg)](https://pkg.go.dev/go.arsenm.dev/infinitime)

---

### Importing

This library's import path is `go.arsenm.dev/infinitime`.

---

### Dependencies

This library requires `dbus`, `bluez`, and `pactl` to function. These allow the library to use bluetooth, control media, control volume, etc.

#### Arch

```shell
sudo pacman -S dbus bluez libpulse --needed
```

#### Debian/Ubuntu

```shell
sudo apt install dbus bluez pulseaudio-utils
```

#### Fedora

```shell
sudo dnf install dbus bluez pulseaudio-utils
```

---

### Features

This library currently supports the following features:

- Notifications
- Heart rate monitoring
- Setting time
- Battery level
- Music control
- OTA firmware upgrades

---

### Mentions

The DFU process used in this library was created with the help of [siglo](https://github.com/alexr4535/siglo)'s source code. Specifically, this file: [ble_dfu.py](https://github.com/alexr4535/siglo/blob/main/src/ble_dfu.py)