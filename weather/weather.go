package weather

import (
	"time"
)

// Type of weather event
type EventType uint8

// These event types correspond to structs
// that can be used to add an event to the
// weather timeline
const (
	EventTypeObscuration EventType = iota
	EventTypePrecipitation
	EventTypeWind
	EventTypeTemperature
	EventTypeAirQuality
	EventTypeSpecial
	EventTypePressure
	EventTypeLocation
	EventTypeClouds
	EventTypeHumidity
)

// Special event type
type SpecialType uint8

// See https://git.io/JM7Oe for the meaning of each type
const (
	SpecialTypeSquall SpecialType = iota
	SpecialTypeTsunami
	SpecialTypeTornado
	SpecialTypeFire
	SpecialTypeThunder
)

// Precipitation type
type PrecipitationType uint8

// See https://git.io/JM7YM for the meaning of each type
const (
	PrecipitationTypeNone PrecipitationType = iota
	PrecipitationTypeRain
	PrecipitationTypeDrizzle
	PrecipitationTypeFreezingRain
	PrecipitationTypeSleet
	PrecipitationTypeHail
	PrecipitationTypeSmallHail
	PrecipitationTypeSnow
	PrecipitationTypeSnowGrains
	PrecipitationTypeIceCrystals
	PrecipitationTypeAsh
)

// Visibility obscuration type
type ObscurationType uint8

// See https://git.io/JM7Yd for the meaning of each type
const (
	ObscurationTypeNone ObscurationType = iota
	ObscurationTypeFog
	ObscurationTypeHaze
	ObscurationTypeSmoke
	ObscurationTypeAsh
	ObscurationTypeDust
	ObscurationTypeSand
	ObscurationTypeMist
	ObscurationTypePrecipitation
)

// TimelineHeader contains the header for a timeline envent
type TimelineHeader struct {
	// UNIX timestamp with timezone offset
	Timestamp uint64
	// Seconds until the event expires
	Expires uint32
	// Type of weather event
	EventType EventType
}

// NewHeader creates and populates a new timeline header
// and returns it
func NewHeader(evtType EventType, expires time.Duration) TimelineHeader {
	now := time.Now()
	_, offset := now.Zone()
	now = now.Add(time.Duration(offset) * time.Second)

	return TimelineHeader{
		Timestamp: uint64(now.Unix()),
		Expires:   uint32(expires.Seconds()),
		EventType: evtType,
	}
}

// CloudsEvent corresponds to EventTypeClouds
type CloudsEvent struct {
	TimelineHeader
	// Cloud coverage percentage
	Amount uint8
}

// ObscurationEvent corresponds to EventTypeObscuration
type ObscurationEvent struct {
	TimelineHeader
	// Type of obscuration
	Type ObscurationType
	// Visibility in meters. 65535 is unspecified.
	Amount uint16
}

// PrecipitationEvent corresponds to EventTypePrecipitation
type PrecipitationEvent struct {
	TimelineHeader
	// Type of precipitation
	Type PrecipitationType
	// Amount of rain in millimeters. 255 is unspecified.
	Amount uint8
}

// WindEvent corresponds to EventTypeWind
type WindEvent struct {
	TimelineHeader
	// Minimum speed in meters per second
	SpeedMin uint8
	// Maximum speed in meters per second
	SpeedMax uint8
	// Unitless direction, about 1 unit per 0.71 degrees.
	DirectionMin uint8
	// Unitless direction, about 1 unit per 0.71 degrees
	DirectionMax uint8
}

// TemperatureEvent corresponds to EventTypeTemperature
type TemperatureEvent struct {
	TimelineHeader
	// Temperature in celcius multiplied by 100.
	// -32768 is "no data"
	Temperature int16
	// Dew point in celcius multiplied by 100.
	// -32768 is "no data"
	DewPoint int16
}

// LocationEvent corresponds to EventTypeLocation
type LocationEvent struct {
	TimelineHeader
	// Location name
	Location string
	// Altitude from sea level in meters
	Altitude int16
	// EPSG:3857 latitude (Google Maps, Openstreetmaps)
	Latitude int32
	// EPSG:3857 longitude (Google Maps, Openstreetmaps)
	Longitude int32
}

// HumidityEvent corresponds to EventTypeHumidity
type HumidityEvent struct {
	TimelineHeader
	// Relative humidity percentage
	Humidity uint8
}

// PressureEvent corresponds to EventTypePressure
type PressureEvent struct {
	TimelineHeader
	// Air pressure in hectopascals (hPa)
	Pressure int16
}

// SpecialEvent corresponds to EventTypeSpecial
type SpecialEvent struct {
	TimelineHeader
	// Type of special event
	Type SpecialType
}

// AirQualityEvent corresponds to EventTypeAirQuality
type AirQualityEvent struct {
	TimelineHeader
	// Name of the polluting item
	//
	// Do not localize the name. That should be handled by the watch.
	//
	// For particulate matter, use "PM0.1"`, "PM5", or "PM10".
	//
	// For chemicals, use the molecular formula ("NO2", "CO2", or "O3").
	//
	// For pollen, use the genus of the plant.
	Polluter string
	// Amount of pollution in SI units
	//
	// https://ec.europa.eu/environment/air/quality/standards.htm
	// http://www.ourair.org/wp-content/uploads/2012-aaqs2.pdf
	Amount uint32
}
