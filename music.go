package infinitime

import "github.com/muka/go-bluetooth/bluez/profile/gatt"

type MusicEvent uint8

const (
	MusicEventChar  = "00000001-78fc-48fe-8e23-433b3a1942d0"
	MusicStatusChar = "00000002-78fc-48fe-8e23-433b3a1942d0"
	MusicArtistChar = "00000003-78fc-48fe-8e23-433b3a1942d0"
	MusicTrackChar  = "00000004-78fc-48fe-8e23-433b3a1942d0"
	MusicAlbumChar  = "00000005-78fc-48fe-8e23-433b3a1942d0"
)

const (
	MusicEventOpen    MusicEvent = 0xe0
	MusicEventPlay    MusicEvent = 0x00
	MusicEventPause   MusicEvent = 0x01
	MusicEventNext    MusicEvent = 0x03
	MusicEventPrev    MusicEvent = 0x04
	MusicEventVolUp   MusicEvent = 0x05
	MusicEventVolDown MusicEvent = 0x06
)

// MusicCtrl stores everything required to control music
type MusicCtrl struct {
	eventChar  *gatt.GattCharacteristic1
	statusChar *gatt.GattCharacteristic1
	artistChar *gatt.GattCharacteristic1
	trackChar  *gatt.GattCharacteristic1
	albumChar  *gatt.GattCharacteristic1
}

// SetStatus sets the playing status
func (mc MusicCtrl) SetStatus(playing bool) error {
	if playing {
		return mc.statusChar.WriteValue([]byte{0x1}, nil)
	}
	return mc.statusChar.WriteValue([]byte{0x0}, nil)
}

// SetArtist sets the artist on InfniTime
func (mc MusicCtrl) SetArtist(artist string) error {
	return mc.artistChar.WriteValue([]byte(artist), nil)
}

// SetTrack sets the track name on InfniTime
func (mc MusicCtrl) SetTrack(track string) error {
	return mc.trackChar.WriteValue([]byte(track), nil)
}

// SetAlbum sets the album on InfniTime
func (mc MusicCtrl) SetAlbum(album string) error {
	return mc.albumChar.WriteValue([]byte(album), nil)
}

// WatchEvents watches music events from InfiniTime
func (mc MusicCtrl) WatchEvents() (<-chan MusicEvent, error) {
	// Start notifications on music event characteristic
	err := mc.eventChar.StartNotify()
	if err != nil {
		return nil, err
	}
	// Watch music event properties
	ch, err := mc.eventChar.WatchProperties()
	if err != nil {
		return nil, err
	}
	musicEventCh := make(chan MusicEvent, 5)
	go func() {
		// For every event
		for event := range ch {
			// If value changes
			if event.Name == "Value" {
				log.Debug().Bytes("value", event.Value.([]byte)).Msg("Received music event from watch")
				// Send music event to channel
				musicEventCh <- MusicEvent(event.Value.([]byte)[0])
			}
		}
	}()
	return musicEventCh, nil
}
